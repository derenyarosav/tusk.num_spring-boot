package com.example.springboot.repository;

import com.example.springboot.data.Info;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class InfoRepository {

    private final Set<String> dataProject = new HashSet<>();
    private final Set<Object> dataInfo = new HashSet<>();
    private final Set<Object> dataFailedResults = new HashSet<>();
    private final Set<Object> dataSuccessResults = new HashSet<>();

    private final Set<Object> dataCompilationTimeMore = new HashSet<>();
    private final Set<Object> dataCompilationTimeLess = new HashSet<>();


    public void putData(Info info) {
        dataProject.add(info.getProject());
        dataInfo.add(info);
        if (!info.getResult().equals("success")) {
            dataFailedResults.add(info);
        }
        if (info.getResult().equals("success")) {
            dataSuccessResults.add(info);
        }
        if (info.getCompilationTime() >= 1000) {
            dataCompilationTimeMore.add(info);
        }
        if (info.getCompilationTime() < 1000) {
            dataCompilationTimeLess.add(info);
        }


    }

    public Set<Object> getAllProjects() {
        return Collections.unmodifiableSet(dataProject);
    }

    public Set<Object> getDataInfo() {
        return dataInfo;
    }

    public Set<Object> getFailedResults() {
        return dataFailedResults;
    }

    public Set<Object> getSuccessResults() {
        return dataSuccessResults;
    }

    public Set<Object> getCompilationTimeMore() {
        return dataCompilationTimeMore;
    }

    public Set<Object> getCompilationTimeLess() {return dataCompilationTimeLess;}

}
