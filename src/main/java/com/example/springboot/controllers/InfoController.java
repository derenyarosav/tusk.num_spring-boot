package com.example.springboot.controllers;


import com.example.springboot.data.Info;
import com.example.springboot.repository.InfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;
import java.util.Set;

@RestController
@RequestMapping()
public class InfoController {
    @Autowired
    private InfoRepository infoRepository;

    public InfoController(InfoRepository infoRepository) {
        this.infoRepository = infoRepository;
    }

    @PostMapping("/register-compilation-time")
    public String putInfo(@RequestBody Info info) {
        infoRepository.putData(info);
        return " project : " + info.getProject() + " timestamp : " + info.getTimestamp() + " compilationTime : " +
                info.getCompilationTime() + " result : " + info.getResult();

    }

    @GetMapping("/projects")
    public Set<Object> getProjects() {
        return infoRepository.getAllProjects();
    }

    @GetMapping("/info")
    public Collection<Object> getInfo() {
        return infoRepository.getDataInfo();
    }

    @GetMapping("/failed-results")
    public Collection<Object> getFailedResults() {
        return infoRepository.getFailedResults();
    }

    @GetMapping("/success-results")
    public Collection<Object> getSuccessResults() {
        return infoRepository.getSuccessResults();
    }

    @GetMapping("/long-compilation")
    public Collection<Object> getCompilationTimeMore() {
        return infoRepository.getCompilationTimeMore();
    }

    @GetMapping("/normal-compilation")
    public Collection<Object> getCompilationTimeLess() {
        return infoRepository.getCompilationTimeLess();
    }

}
